<?php

namespace nerd\Shopaform\Components;

use Cms\Classes\ComponentBase;
use Db;
use System\Models\File;
use GuzzleHttp\Psr7\Request;
use Input;
use Redirect;
use Session;

class selecttype extends ComponentBase
{

    
    public function componentDetails()
    {
        return [
            'name' => 'selecttype',
            'description' => 'Inventaire du résultat de recherche'
        ];
    }


    public function defineProperties()
    {
        // return [
        //     "popularityFilter" => [
        //         "title" => "Add filter by populairity",
        //         "type" => "checkbox",
        //         'default' => 'false',
        //     ],
        // ];
    }

    public function Displaysearch()
    {
        // if(empty($sinputValue))
        // {
        //     return null;
        // }
        //$sinputValue =strtolower($_GET['category']);
        $category = strtolower($_GET['category']);
        $year = strtolower($_GET['year']);
        $model = strtolower($_GET['model']);
        //Products
        $obSetting = Db::select("SELECT DISTINCT nerd_shopaholic_type.id FROM nerd_shopaholic_type INNER JOIN lovata_shopaholic_products on nerd_shopaholic_type.id = lovata_shopaholic_products.type_id where category_id = $category AND year = $year AND model_product = '$model'");
       
       // $obSetting = Db::select("SELECT id FROM lovata_shopaholic_products where category_id = $category AND year = $year AND model_product = '$model'");
        //$obSetting = Db::select("select * from lovata_shopaholic_products  where LOWER(lovata_shopaholic_products.name) LIKE '%$sinputValue%'"  );
        //$obSetting = self::where('item' , 'lovata_site_settings')->first();
       $test =array();
        $testarray = array();
        $MesProduits = array();
        foreach ( $obSetting as $value) {
           $test  = get_object_vars($value);
                array_push($MesProduits, $test['id']);
                //$testarray['product'] = MesProduits$
            
        }
        
        $testarray = ['type' => $MesProduits];
       // $sValue = $obSetting['id'];
        return $testarray;
    }

    public  function onSearch()
    {

        
        $type = Request('type');
        $category = strtolower($_GET['category']);
        $year = strtolower($_GET['year']);
        $model = strtolower($_GET['model']);
        $MyLink = "/piece-inventaire/?category=$category&year=$year&model=$model&type=$type";

            return Redirect::to($MyLink);
        
    }
    

    public function makeItem($idType)
    {
        $obSetting = Db::select("SELECT * FROM nerd_shopaholic_type WHERE id = $idType");

        

        $obImage = File::where('attachment_type', 'Nerd\Shopaholic\Models\Type')->where('field','image')->where('attachment_id' , $idType)->first();
       
        $obSetting += ['myImage' => $obImage];
        return $obSetting;
       
    }

    public function onRun()
    {
          $this->page['year'] = strtolower($_GET['year']);
          $this->page['modele'] = strtolower($_GET['model']);
          $this->page['category'] = strtolower($_GET['category']);

        $this->addCss('/plugins/nerd/shopaform/assets/css/step2.css' , 'core');
        $this->addJs('/plugins/nerd/shopaform/assets/js/step2.js' , 'core');
    }

public function getCategory($id )
    {


        

        $obSetting = Db::select("SELECT * FROM lovata_shopaholic_categories WHERE id = $id");
        
        
     
        return $obSetting;
       
    }


    public function getProperty($propertyName)
    {
        return $this->property($propertyName);
    }

    public function get($sCode, $nIdCategory)
    {
        $type = Input::get('year') ?? '';
        switch ($sCode) {
            case "year":
                $obSetting = Db::select("select DISTINCT year from lovata_shopaholic_products  where category_id ='$nIdCategory' and type_Vehicule='$type'");
                break;
            case "type_Vehicule":
                $obSetting = Db::select("select DISTINCT type_Vehicule from lovata_shopaholic_products  where category_id ='$nIdCategory'");
                break;
        }

        $MesProduits = array();
        foreach ($obSetting as $value) {
            $objectToArray  = get_object_vars($value);
            array_push($MesProduits, $objectToArray[$sCode]);
        }
        return $MesProduits;
    }

    public function getProductListFiltered($nIdCategory, $filter, $IdList)
    {
        switch ($filter) {
            case 0:
                $sCondition = "ORDER BY popularity DESC";
                break;
            case 1:
                $sCondition = "ORDER BY popularity ASC";
                break;
            case 2:
                $sCondition = "ORDER BY year DESC";
                break;
            case 3:
                $sCondition = "ORDER BY year ASC";
                break;
            case 4:
                $sCondition = "ORDER BY lovata_shopaholic_prices.price DESC";
                break;
            case 5:
                $sCondition = "ORDER BY lovata_shopaholic_prices.price ASC";
                break;
            default:
                break;
        }
        $obSetting = Db::select("SELECT * from lovata_shopaholic_products  where category_id ='$nIdCategory'  AND id IN $IdList " . $sCondition );
        return $obSetting;
    }

    public function onFilterList()
    {
        $filter = Request('filter');
        $maListe = Session::get('IdList');
        
        $typeUpdate = 'filter';
        return [
            '#TEST' => $this->renderPartial('@partials/productsList.htm', ['filter' => $filter, 'typeUpdate' => $typeUpdate , 'listId' => $maListe])
        ];
    }
}
