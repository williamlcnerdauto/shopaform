<?php namespace Nerd\Shopaholic\Models;

use Illuminate\Contracts\Support\Jsonable;
use Model;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;

        /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [
        'year', 'model_product', 'product_id','type_id', 'popularity',
    ];
    

     /**
     * @var array hasOne and other relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'product' => ['Lovata\Shopaholic\Models\Product']
    ];
    // public $belongsToMany = ['related_product ' => [
    //     'Nerd\Shopaholic\Models\Product',
    //     'table' => 'lovata_shopaholic_products'
    // ]];
    public $belongsToMany = [''];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = ['related_product'];
    public $jsonable  = ['related_product'];

   



    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'nerd_shopaholic_product';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
