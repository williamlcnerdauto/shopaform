<?php namespace Nerd\Shopaholic\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Lovata\Shopaholic\Models\Product;

/**
 * relatedProductBox Form Widget
 */
class Relatedproductbox extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'nerd_shopaholic_related_product_box';

    /**
     * @inheritDoc
     */
    public function init()
    {
        return 1;
    }

    public function widgetDetails()
    {
        return [
            'name' => 'relatedproductbox',
            'description' => 'Champ pour sélectionner les produits similaires'
        ];
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('relatedproductbox');
    }

    /**
     * prepareVars for view data
     */
    public function prepareVars()
    {
        $this->vars['id'] = $this->model->id;
        $this->vars['category'] = $this->model->category_id;
        $this->vars['related'] = Product::where('category_id' , $this->vars['category'])->where('id' ,'!=' ,$this->model->id )->lists('name','id');
        $this->vars['name'] = $this->formField->getName().'[]';
        

        if(!empty($this->getLoadValue())){
            $this->vars['selectedValues'] = $this->getLoadValue();
        }else{
            $this->vars['selectedValues'] = [];
        }
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/select2.css', 'Nerd.Shopaholic');
        $this->addJs('js/select2.js', 'Nerd.Shopaholic');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}
