<?php namespace Nerd\Shopaholic\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddNewFields extends Migration
{
    public function up()
    {
        Schema::table('lovata_shopaholic_products' ,function($table)
        {
            $table->integer('year')->nullable();
            $table->string('model_product')->nullable();
            $table->integer('type_id')->nullable()->unsigned();
            $table->text('related_product')->nullable();
            $table->integer('popularity')->nullable();
        
    
         });

    }

    public function down()
    {
        Schema::table('lovata_shopaholic_products', function($table)
        {   
            $table->dropColumn('year');
            $table->dropColumn('model_product');
            $table->dropColumn('type_id');
            $table->dropColumn('related_product');
            $table->dropColumn('popularity');
        });
    }



    

}
