<?php

namespace nerd\Shopaholic\Controllers;

use Exception;
use Validator;
use Illuminate\Http\Request;
use RainLab\User\Facades\Auth;
use Backend\Classes\Controller;
use Flash;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Mail;
use Nerd\Shopaholic\Models\Product as produit;

class ProductsController extends  Controller
{

    // public function userExist(Request $request)
    // {
    //     if ($request->email == null || $request->password == null) {
    //         return response()->json(["mesage" => "Please fill all input"], 404);
    //     }
    //     try {
    //         $user = User::where("email", $request->email)->first();
    //         if ($user == null) {
    //             return response()->json(["mesage" => "User don't exist"], 404);
    //         }
    //         if (Hash::check($request->password, $user->password))
    //             $array = array("password" => [$request->password], 'email' => [$user->email]);
    //         $jsona = json_encode($array, JSON_PRETTY_PRINT);
    //         return response()->json(["User" => [
    //             'username' => $user->name
    //         ]], 200);

    //         if (!Hash::check($request->password, $user->password))
    //             return response()->json(["message" => "Invalide password"], 400);
    //     } catch (Exception $err) {
    //         return response()->json(["mesage" => "Internal Error"], 500);
    //     }
    // }

    // public function signIn(){

    //     $user = Auth::getUser();
    //     $loggedIn = $this->account->onSignin();


    //     return response()->json(["message" => $loggedIn], 200);
    // }

//     public function login(Request $request)
//     {
            

//         try {

//             $user = User::where('email', $request->email)->first();
//             if (Hash::check($request->password, $user->password)){
//                 $test =   Auth::login($user,true);
//             $cookie= \Cookie::get('october_session');
//             $cookie2 = json_decode($cookie);


//             return   response()->json(["cookie"=>$cookie,"email"=>$user->email], 201);
// }
//         } catch (Exception $e) {

//             return   response()->json(["response" => $e], 500);
//         }
//     }

    public function CurrentUser()
    {

        $user = Auth::getUser();

        return response()->json($user, 200);
    }



    /**  @param
     * String first_name * String last_name
     * String address; * String city;
     * String zip_code * String payment_method
     * String billing_name * String shipping_name
     * String company * String Company_type
     */

    public function createProduct(Request $request)
    {

        try {
            $produit = produit::where('produit_id', $request->id)->first();

            $object = json_decode($produit);
            foreach ($object as $key => $value) {
                if ($request->$key != null)
                    $produit->$key = $request->$key;
            }
            $produit->save();
            return  response()->json(["mesage" => "Created"], 200);
        } catch (Exception $e) {
            return   response()->json(["mesage" => "Internal Error"], 500);
        }
    }

    // public function subscribe(Request $request)
    // {

    //     try {
    //         $rules = [
    //             'name' => 'required',
    //             'email' => 'required|email|unique:users,email',
    //             'password' => 'required|same:password_confirmation'
    //         ];
    //         $validator = Validator::make($request->all(), $rules);
    //         if ($validator->fails()) {

    //             return response()->json($validator->errors(), 400);
    //         }

    //         $data = [
    //             'name' => $request->name,
    //             'email' => $request->email,
    //             'password' => $request->password,
    //             'password_confirmation' => $request->password_confirmation,
    //         ];
    //         $user =  Auth::register($data, true);
    //         $loggedIn = Auth::check();


    //         Profile::getFromUser($user);

    //         $profile = Profile::where('user_id', $user->id)->first();
    //         $object = json_decode($profile);

    //         foreach ($object as $key => $value) {
    //             if ($request->$key != null)
    //                 $profile->$key = $request->$key;
    //         }
    //         $profile->save();
    //         return  response()->json(["message" => "Created"], 201);
    //     } catch (Exception $e) {
    //         return   response()->json(["message" => $e], 500);
    //     }
    // }


    // public function subscribe_withForm($request)
    // {    
 
    //     try {   
    //         $user = User::create([
    //             'name' => $request['email'],
    //             'email' => $request['email'],
    //             'password' => $request['password'],
    //             'password_confirmation' => $request['password_confirmation'],
    
    //         ]);

    //         Profile::getFromUser($user);
    //         $profile= Profile::where('user_id', $user->id)->first();
    //         $request += ['user_id' => $user->id];
    //         $profile->fill($request);
    //         $profile->save();
    //         return   $user->id;

    //     } catch (Exception $e) {
    //         return  '500|' .  $e->getMessage();
    //     }
       
    // }


    // public function update_withForm($user)
    // {    
 
    //     try {   
    //         $profile= Profile::where('user_id', $user['id'])->first();
    //         $profile->fill($user);
    //         $profile->save();

    //     } catch (Exception $e) {
    //         return  '500|' .  $e->getMessage();
    //     }
       
    // }

    //  /**
    //  * Manually activate a user
    //  */
    // public function preview_onActivate($recordId = null)
    // {
    //     $model = $this->formFindModelObject($recordId);

    //     $model->attemptActivation($model->activation_code);

    //     Flash::success(Lang::get('rainlab.user::lang.users.activated_success'));
    //     $array = (array) $model;

    //     Mail::sendTo('james@nerdmarketing.ca' , ['html' => 'rainlab.user::mail.welcome'], $array, function($message)  { 
    //         $message->subject('welcome');
    //    });
    //     if ($redirect = $this->makeRedirect('update-close', $model)) {
    //         return $redirect;
    //     }
    // }




    
}
